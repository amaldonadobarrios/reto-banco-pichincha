
DROP TABLE IF EXISTS MONEDA;

create table MONEDA (
        ID_MONEDA bigint auto_increment NOT NULL,
        NOMBRE_MONEDA varchar(100) not null,
        SIMBOLO_MONEDA varchar(10) not null,
        PAIS_MONEDA varchar(50) not null,
        primary key (ID_MONEDA)
);

DROP TABLE IF EXISTS OPCION_TIPO_CAMBIO;

create table OPCION_TIPO_CAMBIO (
        ID_OPCION_TIPO_CAMBIO bigint auto_increment NOT NULL,
        NOMBRE_TIPO_OPCION varchar(100) not null,
        primary key (ID_OPCION_TIPO_CAMBIO)
);

DROP TABLE IF EXISTS USUARIOS;

create table USUARIOS (
        ID_USUARIO bigint  auto_increment NOT NULL,
        NOMBRES varchar(100) not null,
		APELLIDOS varchar(100) not null,
		DNI varchar(100) not null,
		USUARIO varchar(100) not null,
		PASSWORD varchar(500) not null,
		ESTADO bit not null,
        primary key (ID_USUARIO)
);

DROP TABLE IF EXISTS ROLES;

create table ROLES (
        ID_ROL bigint auto_increment NOT NULL,
        NOMBRE varchar(100) not null,
        primary key (ID_ROL)
);

DROP TABLE IF EXISTS USUARIOS_ROLES;

create table USUARIOS_ROLES (
        ID_USUARIO bigint NOT NULL,
        ID_ROL  bigint NOT NULL,
        primary key (ID_USUARIO,ID_ROL)
);

DROP TABLE IF EXISTS CLIENTES;

create table CLIENTES (
        ID_CLIENTE bigint auto_increment NOT NULL,
        NOMBRES varchar(100) not null,
		APELLIDOS varchar(100) not null,
		DNI varchar(100) not null,
        primary key (ID_CLIENTE)
);

DROP TABLE IF EXISTS TIPOS_CAMBIO;

create table TIPOS_CAMBIO (
        ID_TIPO_CAMBIO bigint auto_increment NOT NULL,
        ID_OPCION_TIPO_CAMBIO bigint not null,
		ID_MONEDA_ORIGEN bigint not null,
		ID_MONEDA_DESTINO bigint not null,
		VALOR_CAMBIO DECIMAL(5,2)  not null,
		ID_USUARIO_REGISTRA bigint not null,
		FECHA_REGISTRO DATETIME(6) default CURRENT_TIMESTAMP,
		ID_USUARIO_ACTUALIZA bigint,
		FECHA_ACTUALIZA  DATETIME(6),
        primary key (ID_TIPO_CAMBIO)
);

DROP TABLE IF EXISTS OPERACIONES_TIPO_CAMBIO;

create table OPERACIONES_TIPO_CAMBIO (
        ID_OPERACIONES_TIPO_CAMBIO bigint auto_increment NOT NULL,
        ID_TIPO_CAMBIO bigint not null,
		MONTO_ORIGEN DECIMAL(10,2)  not null,
		MONTO_DESTINO DECIMAL(10,2)  not null,
		ID_USUARIO_REGISTRA bigint not null,
		FECHA_REGISTRO DATETIME(6)  not null,
		ID_CLIENTE bigint not null,
        primary key (ID_OPERACIONES_TIPO_CAMBIO)
);

DROP TABLE IF EXISTS AUDITORIA;

create table AUDITORIA (
        ID_AUDITORIA bigint auto_increment NOT NULL,
        OPERACION varchar(100) not null,
		METODO varchar(100) not null,
		PARAMETOS_ENTRADA varchar(1000) not null,
		PARAMETOS_SALIDA varchar(1000) not null,
		FECHA_REGISTRO DATE  not null,
		ID_USUARIO bigint not null,
        primary key (ID_AUDITORIA)
);





