package com.pe.bancopichincha.api.request;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TipoCambioUpdateRequest {

	private Integer idTipoCambio;
	private BigDecimal valorCambio;
	private Integer codeMonedaOrigen;
	private Integer codeMonedaDestino;
	private Integer opcionTipoCambio;
	private Integer usuario;
	
}
