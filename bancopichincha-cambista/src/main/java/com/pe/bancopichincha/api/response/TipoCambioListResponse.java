package com.pe.bancopichincha.api.response;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TipoCambioListResponse {

	private BigDecimal valorCambio;
	private String simboloMonedaOrigen;
	private String simboloMonedaDestino;
	private String opcionTipoCambio;
	private String idUsuarioRegistra;
	private String idUsuarioActualiza;
	private String fechaActualiza;
	private String fechaRegistra;
}
