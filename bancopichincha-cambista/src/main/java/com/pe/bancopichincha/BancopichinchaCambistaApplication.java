package com.pe.bancopichincha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BancopichinchaCambistaApplication {

	public static void main(String[] args) {
		SpringApplication.run(BancopichinchaCambistaApplication.class, args);
	}

}
