package com.pe.bancopichincha.controller;

import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pe.bancopichincha.api.request.TipoCambioSaveRequest;
import com.pe.bancopichincha.api.request.TipoCambioUpdateRequest;
import com.pe.bancopichincha.model.entity.TiposCambio;
import com.pe.bancopichincha.service.TiposCambioService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/tipocambio")
public class TipoCambioController {

	@Autowired
	TiposCambioService tiposCambioService;

	@GetMapping("/listar")
	public Flux<TiposCambio> obtenerTodosTipoCambio() {
		return tiposCambioService.listarTiposCambio();
	}

	@PostMapping("/save")
	public Mono<TiposCambio> crearTipoCambio(@RequestBody TipoCambioSaveRequest request) {
		return tiposCambioService.saveTiposCambio(request);
	}
	
    @PutMapping("/edit")
    public Mono<TiposCambio> edit(@RequestBody TipoCambioUpdateRequest request) { 
        return tiposCambioService.updateTiposCambio(request);
    }
    
    @PostMapping("/delete/{id}")
    public Mono<Void> delete(@PathVariable final String id) throws InterruptedException, ExecutionException {
    	return tiposCambioService.eliminarTiposCambio(Long.parseLong(id));
    }
}
