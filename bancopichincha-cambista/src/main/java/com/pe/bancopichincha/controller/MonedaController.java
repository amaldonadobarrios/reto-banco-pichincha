package com.pe.bancopichincha.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pe.bancopichincha.model.dto.MonedaDTO;
import com.pe.bancopichincha.service.MonedaService;

import reactor.core.publisher.Flux;


@RestController
@RequestMapping("/moneda")
public class MonedaController {

    @Autowired
	MonedaService MonedaService;

    @GetMapping("/listar")
    public Flux<MonedaDTO> findAll() {
        return MonedaService.findAll();
      }
       


}
