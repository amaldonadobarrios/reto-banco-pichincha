package com.pe.bancopichincha.controller;

import java.util.Arrays;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.log4j.Log4j2;
import reactor.core.publisher.Flux;

@Log4j2
@RestController 
@RequestMapping("cambiodivisas")
@SpringBootApplication
public class OperacionCambioDivisasController {

    @GetMapping
    public Flux<Integer> getAllProducts() {
    	log.info("All products request");
    	var flux = Flux.fromIterable(Arrays.asList(10,20,30,40,50)); 
        return flux;
    }
	
}
