package com.pe.bancopichincha.model.entity;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * The persistent class for the CLIENTES database table.
 * 
 */
@Table("CLIENTES")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Cliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column("ID_CLIENTE")
	private long idCliente;

	@Column("APELLIDOS")
	private String apellidos;

	@Column("DNI")
	private String dni;

	@Column("NOMBRES")
	private String nombres;


	

}