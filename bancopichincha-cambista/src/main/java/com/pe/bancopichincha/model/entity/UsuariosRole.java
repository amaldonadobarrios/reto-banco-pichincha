package com.pe.bancopichincha.model.entity;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * The persistent class for the USUARIOS_ROLES database table.
 * 
 */
@Table("USUARIOS_ROLES")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UsuariosRole implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column("ID_ROL")
	private long idRol;

	@Column("ID_USUARIO")
	private Usuario usuario;

	
}