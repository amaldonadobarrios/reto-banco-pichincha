package com.pe.bancopichincha.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * The persistent class for the TIPOS_CAMBIO database table.
 * 
 */

@Table("TIPOS_CAMBIO")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TiposCambio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private long idTipoCambio;

	@Column("FECHA_ACTUALIZA")
	private LocalDate  fechaActualiza;

	@Column("FECHA_REGISTRO")
	private LocalDate  fechaRegistro;

	@Column("ID_USUARIO_ACTUALIZA")
	private long idUsuarioActualiza;

	@Column("VALOR_CAMBIO")
	private BigDecimal valorCambio;

	@Column("ID_MONEDA_ORIGEN")
	private long monedaOrigen;

	@Column("ID_MONEDA_DESTINO")
	private long monedaDestino;

	@Column("ID_OPCION_TIPO_CAMBIO")
	private long opcionTipoCambio;

	@Column("ID_USUARIO_REGISTRA")
	private long usuario;

	

}