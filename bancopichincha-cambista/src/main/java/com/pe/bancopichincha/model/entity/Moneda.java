package com.pe.bancopichincha.model.entity;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * The persistent class for the MONEDA database table.
 * 
 */

@Table(name="MONEDA")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Moneda implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column("ID_MONEDA")
	private long idMoneda;

	@Column("NOMBRE_MONEDA")
	private String nombreMoneda;

	@Column("PAIS_MONEDA")
	private String paisMoneda;

	@Column("SIMBOLO_MONEDA")
	private String simboloMoneda;

	
}