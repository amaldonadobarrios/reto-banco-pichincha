package com.pe.bancopichincha.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MonedaDTO {

	private Long idMoneda;
	
	private String nombreMoneda;
	
	private String paisMoneda;

	private String simboloMoneda;
	
}
