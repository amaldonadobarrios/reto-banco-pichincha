package com.pe.bancopichincha.model.entity;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * The persistent class for the USUARIOS database table.
 * 
 */

@Table("USUARIOS")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Usuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column("ID_USUARIO")
	private long idUsuario;

	@Column("APELLIDOS")
	private String apellidos;

	@Column("DNI")
	private String dni;

	@Column("ESTADO")
	private boolean estado;

	@Column("NOMBRES")
	private String nombres;

	@Column("PASSWORD")
	private String password;

	@Column("USUARIO")
	private String usuario;


	

}