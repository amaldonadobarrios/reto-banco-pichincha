package com.pe.bancopichincha.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * The persistent class for the OPERACIONES_TIPO_CAMBIO database table.
 * 
 */

@Table("OPERACIONES_TIPO_CAMBIO")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OperacionesTipoCambio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column("ID_OPERACIONES_TIPO_CAMBIO")
	private long idOperacionesTipoCambio;

	@Column("FECHA_REGISTRO")
	private LocalDate fechaRegistro;

	@Column("MONTO_DESTINO")
	private BigDecimal montoDestino;

	@Column("MONTO_ORIGEN")
	private BigDecimal montoOrigen;

	//bi-directional many-to-one association to Cliente
	@Column("ID_CLIENTE")
	private long cliente;

	//bi-directional many-to-one association to TiposCambio

	@Column("ID_TIPO_CAMBIO")
	private long tiposCambio;

	//bi-directional many-to-one association to Usuario

	@Column("ID_USUARIO_REGISTRA")
	private long usuario;

	
}