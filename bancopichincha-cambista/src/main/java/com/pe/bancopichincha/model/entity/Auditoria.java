package com.pe.bancopichincha.model.entity;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The persistent class for the AUDITORIA database table.
 * 
 */
@Table("AUDITORIA")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Auditoria implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column("ID_AUDITORIA")
	private long idAuditoria;

	@Column("FECHA_REGISTRO")
	private Object fechaRegistro;

	@Column("ID_USUARIO")
	private long idUsuario;

	@Column("METODO")
	private String metodo;

	@Column("OPERACION")
	private String operacion;

	@Column("PARAMETOS_ENTRADA")
	private String parametosEntrada;

	@Column("PARAMETOS_SALIDA")
	private String parametosSalida;

}