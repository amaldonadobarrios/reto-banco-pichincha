package com.pe.bancopichincha.model.entity;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * The persistent class for the OPCION_TIPO_CAMBIO database table.
 * 
 */

@Table("OPCION_TIPO_CAMBIO")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OpcionTipoCambio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column("ID_OPCION_TIPO_CAMBIO")
	private long idOpcionTipoCambio;

	@Column("NOMBRE_TIPO_OPCION")
	private String nombreTipoOpcion;


}