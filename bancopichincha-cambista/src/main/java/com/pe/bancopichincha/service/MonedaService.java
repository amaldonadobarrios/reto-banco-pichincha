package com.pe.bancopichincha.service;

import com.pe.bancopichincha.model.dto.MonedaDTO;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


public interface MonedaService {

	Flux<MonedaDTO> findAll();

    Mono<MonedaDTO> get(Long id);

}
