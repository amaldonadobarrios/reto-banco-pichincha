package com.pe.bancopichincha.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pe.bancopichincha.model.dto.MonedaDTO;
import com.pe.bancopichincha.model.entity.Moneda;
import com.pe.bancopichincha.repository.MonedaRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@Service
public class MonedaServiceImpl implements MonedaService {

    @Autowired
	MonedaRepository monedaRepository;

    @Override
	public Flux<MonedaDTO> findAll() {
		return monedaRepository.findAll()
	    .doOnNext(p -> this.mapToDTO( p))
	    .flatMap(this::mapToDTO);
	}
    
    @Override
    public Mono<MonedaDTO> get(final Long id) {
    	return monedaRepository.findById(id).flatMap(this::mapToDTO);
    }

    private Mono<MonedaDTO> mapToDTO(final Moneda moneda) {
    	final MonedaDTO monedaResponse = new MonedaDTO();
    	monedaResponse.setIdMoneda(moneda.getIdMoneda());
    	monedaResponse.setNombreMoneda(moneda.getNombreMoneda());
    	monedaResponse.setPaisMoneda(moneda.getPaisMoneda());
    	monedaResponse.setSimboloMoneda(moneda.getSimboloMoneda());
        return Mono.just(monedaResponse);
    }

	

}
