package com.pe.bancopichincha.service.impl;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pe.bancopichincha.api.request.TipoCambioSaveRequest;
import com.pe.bancopichincha.api.request.TipoCambioUpdateRequest;
import com.pe.bancopichincha.model.entity.TiposCambio;
import com.pe.bancopichincha.repository.TiposCambioRepository;
import com.pe.bancopichincha.service.TiposCambioService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
@Service
public class TiposCambioServiceImpl implements TiposCambioService {

	@Autowired
	TiposCambioRepository tiposCambioRepository;
	
	@Override
	public Mono<TiposCambio> saveTiposCambio(TipoCambioSaveRequest request) {
		return tiposCambioRepository.save(maptoEntity(request));
	}

	@Override
	public Flux<TiposCambio> listarTiposCambio() {
		return tiposCambioRepository.findAll();
	}
	
	@Override
	public Mono<TiposCambio> updateTiposCambio(TipoCambioUpdateRequest request) {
		return tiposCambioRepository.findById(Long.valueOf(request.getIdTipoCambio())).doOnNext(p -> this.maptoEntity(request, p))
	    .flatMap(this.tiposCambioRepository::save).doOnError(t -> new Exception("No se pudo actulizar")).single();
	
	}
	
	@Override
	public Mono<Void> eliminarTiposCambio(Long id) {
		return tiposCambioRepository.deleteById(id);
	}
	
	private TiposCambio maptoEntity(TipoCambioSaveRequest request){
		TiposCambio entity= new TiposCambio();
		entity.setOpcionTipoCambio(request.getOpcionTipoCambio());
		entity.setMonedaDestino(request.getCodeMonedaDestino());
		entity.setMonedaOrigen(request.getCodeMonedaOrigen());
		entity.setUsuario(request.getUsuario());
		entity.setValorCambio(request.getValorCambio());
		entity.setFechaRegistro(LocalDate.now());
		return entity;
	}
	private TiposCambio maptoEntity(TipoCambioUpdateRequest request,TiposCambio entity){
		entity.setOpcionTipoCambio(request.getOpcionTipoCambio());
		entity.setMonedaDestino(request.getCodeMonedaDestino());
		entity.setMonedaOrigen(request.getCodeMonedaOrigen());
		entity.setIdUsuarioActualiza(request.getUsuario());
		entity.setValorCambio(request.getValorCambio());
		entity.setFechaActualiza(LocalDate.now());
		return entity;
	}




	
}
