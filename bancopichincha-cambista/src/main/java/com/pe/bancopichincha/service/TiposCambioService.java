package com.pe.bancopichincha.service;

import com.pe.bancopichincha.api.request.TipoCambioSaveRequest;
import com.pe.bancopichincha.api.request.TipoCambioUpdateRequest;
import com.pe.bancopichincha.model.entity.TiposCambio;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface TiposCambioService {

	Mono<TiposCambio> saveTiposCambio(TipoCambioSaveRequest request);
	Flux<TiposCambio> listarTiposCambio();
	Mono<TiposCambio> updateTiposCambio(TipoCambioUpdateRequest request);
	Mono<Void> eliminarTiposCambio(Long id);
}
