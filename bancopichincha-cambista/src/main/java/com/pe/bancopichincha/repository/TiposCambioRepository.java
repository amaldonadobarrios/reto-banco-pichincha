package com.pe.bancopichincha.repository;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

import com.pe.bancopichincha.model.entity.TiposCambio;

@Repository
public interface TiposCambioRepository extends ReactiveCrudRepository<TiposCambio, Long> {
}
