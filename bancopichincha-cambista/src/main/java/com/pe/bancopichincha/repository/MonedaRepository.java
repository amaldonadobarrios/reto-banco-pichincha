package com.pe.bancopichincha.repository;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

import com.pe.bancopichincha.model.entity.Moneda;

import reactor.core.publisher.Mono;

@Repository
public interface MonedaRepository extends ReactiveCrudRepository<Moneda, Long> {
    Mono<Moneda> findBynombreMoneda(String nombreMoneda);
}
